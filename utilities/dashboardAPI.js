var DashboardUtilities = require('./DashboardUtilities');
var FormHandling = require('../common/FormHandling');

module.exports.dashboardSummaryHandler = function(req, res){
	let queryString = req.query;
	if(queryString['filter']){queryString = FormHandling.reportFilterToQuerystring(queryString, queryString['filter']);}
	//console.log('dashboardSummaryHandler:queryString', queryString);
	DashboardUtilities.dashboardSummary(queryString, function(result){
		//console.log('dashboardSummaryHandler', 'dashboardSummary:result', result);
		if(result.errorcode){return res.status(400).json(result);}
		else{res.send(result);}
	});
};