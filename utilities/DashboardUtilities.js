var MongoUtilities = require('../common/MongoDbUtilities');
var RedisUtilities = require('../common/RedisUtilities');

let ticketSummaryData =  async function (queryString, callBack) {
    let providerid = ['manishkr', 'maanishcr']
    console.log('providerid==>', providerid);
    let date = new Date(),
      errors = false,
      data = [];
  
  for(let i=0 ; i< providerid.length; i++){
    let eachItem = {};
    let filterdata = {};
    filterdata = { 'providerid': providerid[i], 'type': 'total', 'summary': 'true' };
     await data.push(MongoUtilities.dashboardMongoDbHandler(filterdata, eachItem, null));
    
    eachItem = {};
    let tempDate = new Date();
    tempDate.setDate(date.getDate() - 1);
    filterdata = { 'providerid': providerid[i], 'type': 'dau', 'summary': 'true', 'date': tempDate };
    await data.push(MongoUtilities.dashboardMongoDbHandler(filterdata, eachItem, null));
  
    eachItem = {};
    tempDate = new Date();
    tempDate.setDate(date.getDate() - 7);
    filterdata = { 'providerid': providerid[i], 'type': 'wau', 'summary': 'true', 'date': tempDate };
    await data.push(MongoUtilities.dashboardMongoDbHandler(filterdata, eachItem, null));
  
    eachItem = {};
    tempDate = new Date();
    tempDate.setDate(date.getDate() - 30);
    filterdata = { 'providerid': providerid[i], 'type': 'mau', 'summary': 'true', 'date': tempDate };
    await data.push(MongoUtilities.dashboardMongoDbHandler(filterdata, eachItem, null));
  }
    if (errors) {
      return callBack(errors);
    }
    Promise.all(data).then(data => {
      console.log('data===>', data);
    let finaldata = {};
    let opencount = {};
    let closecount = {};
    let newcount = {};
    let indexArr = data.filter((item)=> item.index === 'total')
    let dauArr = data.filter((item)=> item.index === 'dau')
    let wauArr = data.filter((item)=> item.index === 'wau')
    let mauArr = data.filter((item)=> item.index === 'mau')
    
    for (let index in data) {
      if (data[index]['index'] == 'total') {
        finaldata.newcount = indexArr.reduce((accum,item) => accum + item.newcount, 0)
        finaldata.opencount = indexArr.reduce((accum,item) => accum + item.opencount, 0)
        finaldata.closedcount = indexArr.reduce((accum,item) => accum + item.closedcount, 0)
        finaldata.totalcount = indexArr.reduce((accum,item) => accum + item.totalcount, 0)
      }
      else if (data[index]['index'] == 'dau') {
        opencount.dau = dauArr.reduce((accum,item) => accum + item.opencount, 0)
        closecount.dau = dauArr.reduce((accum,item) => accum + item.closedcount, 0)
        newcount.dau = dauArr.reduce((accum,item) => accum + item.newcount, 0)
      }
      else if (data[index]['index'] == 'wau') {
        opencount.wau = wauArr.reduce((accum,item) => accum + item.opencount, 0)
        closecount.wau = wauArr.reduce((accum,item) => accum + item.closedcount, 0)
        newcount.wau = wauArr.reduce((accum,item) => accum + item.newcount, 0)
      }
      else if (data[index]['index'] == 'mau') {
        opencount.mau = mauArr.reduce((accum,item) => accum + item.opencount, 0)
        closecount.mau = mauArr.reduce((accum,item) => accum + item.closedcount, 0)
        newcount.mau = mauArr.reduce((accum,item) => accum + item.newcount, 0)
      }
    }

    finaldata.open = opencount;
    finaldata.closed = closecount;
    finaldata.new = newcount;

    return callBack(finaldata);
        // console.log("finalData===>", finaldata)
    }).catch(err => {
      return callBack(err);
    });
  };


  module.exports.dashboardSummary = function (queryString, callback) {
    let dashboardSummary = {};
      ticketSummaryData(queryString, function (ticketsummary) {
        if (ticketsummary.errorcode) { return callback(ticketsummary); }
        dashboardSummary['ticket'] = ticketsummary;
        RedisUtilities.setRedisKey(queryString.redisClient, 'tickets', JSON.stringify(dashboardSummary),(redisStatus=>{
          if(redisStatus.errorcode) return callback(redisStatus);
          return callback(dashboardSummary);
        }))
      });
  };
