const serverless = require('serverless-http');
const express = require('express')
const cors = require('cors');
const app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(cors())

const redisClient = require('./common/RedisPool');
app.use(function (req, res, next){req.query.redisClient = redisClient; next();});

app.use('/provider', require('./routes/provider'));

app.all('*', (req, res, next) => {
  res.status(404).json({message: `Can't find ${req.originalUrl} on the server`});
  next();
});

module.exports.handler = serverless(app);