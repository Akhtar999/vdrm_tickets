'use strict';

let FormHandling = require('../common/FormHandling');
const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const MongodbUrl = process.env.localhost ;
const MongodbName = 'admin';
const MongodbOptions = {useUnifiedTopology: true};

const mongoDbErrorCodes = function(errorcode){
    let errorlist = {};
    errorlist['7501'] = 'MongoDb Connection Failed';
    errorlist['7502'] = 'MongoDb Connection Pool Failed';
    errorlist['7503'] = 'MongoDb Insert Failed';
    errorlist['7504'] = 'MongoDb Update Failed';
    errorlist['7505'] = 'MongoDb Select Failed';
    errorlist['7506'] = 'MongoDb Increment Failed';
    errorlist['7507'] = 'MongoDb Count Failed';
    errorlist['7508'] = 'No Such Data Found';
    errorlist['7509'] = 'Nothing To Update';
    return FormHandling.formatErrorCode(errorcode, errorlist);
};

const insertManyDocuments = function(mongocollection, insertData, callback) {
    //console.log('insertManyDocuments:insertData',insertData);
    try{
        insertData.created = new Date();
        mongocollection.insertMany(insertData, function(err, result) {
            assert.equal(err, null);
            callback(result);
        });
    }catch (error) {
        //console.log('insertManyDocuments:Exception',error);
        let errorData = mongoDbErrorCodes(7503);
        errorData.reason = error;
        return callback(errorData);
    }
};

const insertManyMongoDbHandler = function(insertData, queryString, callback) {
    //console.log('insertManyMongoDbHandler:insertData',insertData);
    try {
        MongoClient.connect(MongodbUrl, MongodbOptions, function(err, client) {
            assert.equal(null, err);
            //console.log('Connected successfully to Mongo Db server');
            const mongodbcon = client.db(MongodbName);
            const mongocollection = mongodbcon.collection(queryString['providerid']);
            insertManyDocuments(mongocollection,insertData,function(status){
                //console.log('insertManyMongoDbHandler:insertDocuments',status);
                //console.log('insertManyMongoDbHandler:insertedCount',status.insertedCount);
                client.close();
                let finalResponse = {'success':status['insertedIds'][0]};
                return callback(finalResponse);
            });
        });
    } catch (error) {
        //console.log('insertManyMongoDbHandler:Exception',error);
        let errorData = mongoDbErrorCodes(7503);
        errorData.reason = error;
        return callback(errorData);
    }
};

var simplifyFilterData = function(filterData){
    //console.log('simplifyFilterData:filterData',filterData);
    let searchData = {};
    
    if((filterData['requeststatus']) && (filterData['requeststatus']=== 'OPEN')){
        searchData['$or'] = [{'requeststatus':'CREATED'}, {'requeststatus':'ASSIGNED' }];
        filterData['requeststatus'] = null;
    }

    if(filterData['listType']){searchData['listType'] = filterData['listType'];}
    if(filterData['_id']){searchData['_id'] = filterData['_id'];}
    if(filterData['requestid']){searchData['requestid'] = { $regex: new RegExp(filterData['requestid'], 'i') };}
    if(filterData['title']){searchData['title'] = { $regex: new RegExp(filterData['title'], 'i') };}
    if(filterData['requeststatus']){searchData['requeststatus'] = filterData['requeststatus'].toUpperCase();}
    if(filterData['priority']){searchData['priority'] = filterData['priority'].toUpperCase();}
    if(filterData['category']){searchData['category'] = filterData['category'].toUpperCase();}
    if(filterData['assignee']){searchData['assignee'] = filterData['assignee'];}
    if(filterData['initiator']){searchData['initiator'] = filterData['initiator'];}
    if(filterData['subscriberid']){searchData['subscriberid'] = filterData['subscriberid'];}

    //Handling From Date
    if((filterData['fromdate']) && (!filterData['todate'])){
        filterData['todate'] = filterData['fromdate'];
    }

    //Handling To Date
    if((filterData['todate']) && (!filterData['fromdate'])){
        filterData['fromdate'] = filterData['todate'];
    }

    //From Date and To Date Filter
    if((filterData['fromdate']) && (filterData['todate'])){
        let todate = filterData['todate']+' 23:59:59';
        todate = new Date(todate);
        todate = todate.toLocaleString();
        searchData['created'] = { $gte: new Date(filterData['fromdate']), $lte: new Date(todate)};
    }

    if((filterData['updatedon']) && (typeof(filterData['updatedon']) === 'string')){
        let updatedon_1 = filterData['updatedon'];
        let updatedon_2 = filterData['updatedon'] + ' 23:59:59';
        updatedon_2 = new Date(updatedon_2);
        updatedon_2 = updatedon_2.toLocaleString();
        searchData['updatedon'] = { $gte: new Date(updatedon_1), $lte: new Date(updatedon_2)};
    }

    //Final Serach Data
    return searchData;
};

const findDocuments = function(mongocollection, filterData, callback) {
    //console.log('findDocuments:filterData',filterData);
    
    //searchData
    let searchData = simplifyFilterData(filterData);
    //console.log('findDocuments:filterData',filterData);
    
    //Calculate Page Limit
    let page = 1;
    let pageSize = 15;
    if(filterData['page']){page = parseInt(filterData['page']);}
    if(filterData['pagesize']){pageSize = parseInt(filterData['pagesize']);}
    let skipLimit = ((page-1) * pageSize);
    
    try{
        let skipFields = {projection: {_id: 0, listType: 0 }};
        console.log('findDocuments:searchData', page, pageSize, skipLimit, searchData, skipFields);
        mongocollection.find(searchData,skipFields).sort({created: -1}).skip(skipLimit).limit(pageSize).toArray(function(err, docs) {
            assert.equal(err, null);
            if(docs.length <1){return callback(mongoDbErrorCodes(7508));}
            else{callback(docs);}
        });
    }catch (error) {
        //console.log('findDocuments:Exception',error);
        let errorData = mongoDbErrorCodes(7505);
        errorData.reason = error;
        return callback(errorData);
    }  
};

const getMongoDbHandler = function(queryString, callback) {
    //console.log('getMongoDbHandler:queryString',queryString);
    try {
        MongoClient.connect(MongodbUrl, MongodbOptions, function(err, client) {
            assert.equal(null, err);
            //console.log('Connected successfully to Mongo Db server');
            const mongodbcon = client.db(MongodbName);
            const mongocollection = mongodbcon.collection(queryString['providerid']);
            findDocuments(mongocollection,queryString,function(status){
                //console.log('getMongoDbHandler:findDocuments',status);
                client.close();
                return callback(status);
            });
        });
    } catch (error) {
        //console.log('getMongoDbHandler:Exception',error);
        let errorData = mongoDbErrorCodes(7505);
        errorData.reason = error;
        return callback(errorData);
    }
};

const getDocumentsCount = function(mongocollection, filterData, callback) {
    try{
        let searchData = simplifyFilterData(filterData);
        //console.log('getDocumentsCount:filterData', searchData);
        mongocollection.find(searchData).count(function(err, docs) {
            //console.log('getDocumentsCount:count', docs);
            assert.equal(err, null);
            callback(docs);
        });
    }catch (error) {
        //console.log('getDocumentsCount:Exception',error);
        let errorData = mongoDbErrorCodes(7507);
        errorData.reason = error;
        return callback(errorData);
    }  
};

const listMongoDbHandler = function(queryString, callback) {
    //console.log('listMongoDbHandler:queryString',queryString);
    try {
        MongoClient.connect(MongodbUrl, MongodbOptions, function(err, client) {
            assert.equal(null, err);
            //console.log('Connected successfully to Mongo Db server');
            const mongodbcon = client.db(MongodbName);
            const mongocollection = mongodbcon.collection(queryString['providerid']);
            findDocuments(mongocollection,queryString,function(doclist){
                if(doclist.errorcode){return callback(doclist);}
                getDocumentsCount(mongocollection,queryString,function(doccount){
                    if(doccount.errorcode){return callback(doccount);}
                    client.close();
                    let finalResponse = {};
                    finalResponse.totalcount = doccount;
                    finalResponse.data = doclist;
                    return callback(finalResponse);
                });
            });
        });
    } catch (error) {
        //console.log('listMongoDbHandler:Exception',error);
        let errorData = mongoDbErrorCodes(7505);
        errorData.reason = error;
        return callback(errorData);
    }
};

const incrementDocument = function(mongocollection, filterData, incrementData, callback) {
    //console.log('incrementDocument:filterData',filterData);
    //console.log('incrementDocument:incrementData',incrementData);
    try {
        mongocollection.update(filterData,{$inc: incrementData}, function(err, result) {
            //console.log('incrementDocument:update',result);
            assert.equal(err, null);
            assert.equal(1, result.result.n);
            let finalResponse = {'success':filterData['_id']};
            callback(finalResponse);
        });        
    } catch (error) {
        //console.log('incrementDocument:Exception',error);
        let errorData = mongoDbErrorCodes(7506);
        errorData.reason = error;
        return callback(errorData);
    }
};

const updateDocument = function(mongocollection, filterData, updateData, callback) {
    //console.log('updateDocument:filterData',filterData);
    //console.log('updateDocument:updateData',updateData);
    try {
        mongocollection.updateOne(filterData,{$set:updateData}, function(err, result) {
            assert.equal(err, null);
            assert.equal(1, result.result.n);
            let finalResponse = {'success':filterData['_id']};
            callback(finalResponse);
        });        
    } catch (error) {
        //console.log('updateDocument:Exception',error);
        let errorData = mongoDbErrorCodes(7504);
        errorData.reason = error;
        return callback(errorData);
    }
};

const updateMongoDbHandler = function(filterData, updateData, incrementData, queryString, callback) {
    //console.log('updateMongoDbHandler:filterData',filterData);
    //console.log('updateMongoDbHandler:updateData',updateData);
    //console.log('updateMongoDbHandler:incrementData',incrementData);

    let updateDataCount = Object.keys(updateData).length;
    let incrementDataCount = Object.keys(incrementData).length;
    
    try {
        MongoClient.connect(MongodbUrl, MongodbOptions, function(err, client) {
            assert.equal(null, err);
            const mongodbcon = client.db(MongodbName);
            const mongocollection = mongodbcon.collection(queryString['providerid']);

            if((updateDataCount >=1) && (incrementDataCount >=1)){
                updateDocument(mongocollection,filterData,updateData,function(docstatus){
                    if(docstatus.errorcode){return callback(docstatus);}
                    incrementDocument(mongocollection,filterData,incrementData,function(incstatus){client.close();return callback(incstatus);});
                });}
            else if(updateDataCount >=1){updateDocument(mongocollection,filterData,updateData,function(docstatus){client.close();return callback(docstatus);});}
            else if(incrementDataCount >=1){incrementDocument(mongocollection,filterData,incrementData,function(incstatus){client.close();return callback(incstatus);});}
            else{return callback(mongoDbErrorCodes(7509));}
        });
    } catch (error) {
        //console.log('updateMongoDbHandler:Exception',error);
        let errorData = mongoDbErrorCodes(7504);
        errorData.reason = error;
        return callback(errorData);
    }
};

var simplifyFilterDataDashboard = function(filterData){
    console.log('simplifyFilterData:filterData',filterData);

    let month=null;
    let year=null;
    let day=null;
    let tempdate=null;

    if(filterData['month'])
    {
         month=filterData['month'];
    }
    if(filterData['year'])
    {
         year=filterData['year'];
    }
    if(filterData['day'])
    {
         day=filterData['day'];
    }
    if(filterData['date'])
    {
         tempdate=filterData['date'];
    }
    //let category='DOG';
    let pipeline=[];

    console.log('we r here');


    // const pipeline = [
    //     {
    //       '$match': {
    //         '$and':[{'category': {'$eq': category}},{'priority': {'$eq': 'HIGH'}}]
    //       }
    //     },
    //     {
    //       '$group': {'_id':'$requeststatus','count': {'$sum': 1}}
    //     }
    //   ];

    
    // const pipeline = [
    //     {
    //       '$group': {'_id':'$requeststatus','count': {'$sum': 1}}
    //     }
    //   ];


    // const pipeline = [
    //     {
    //       '$match': {
    //         '$and':[{'month': {'$eq': 10}},{'year': {'$eq': 2020}}]
    //       }
    //     }, 
    //     {
    //       '$group': {'_id': {'requeststatus': '$requeststatus','count': {'$sum': 1}}}
    //     }
    //   ];


    // const pipeline = [
    //     {
    //       '$match': {
    //         'month': {'$eq': 10}
    //       }
    //     }, 
    //     {
    //       '$group': {'_id': {'requeststatus': '$requeststatus','count': {'$sum': 1}}}
    //     }
    //   ];

    if(filterData['type']=='month')
    {
        console.log('we r here 1');
         pipeline = [
        {
          '$match': {
            '$and':[{'month': {'$eq': month}},{'year': {'$eq': year}}]
          }
        }, 
        {
          '$group': {'_id': '$requeststatus','count': {'$sum': 1}}
        }
      ];

    }
    else if(filterData['type']=='year')
    {
        console.log('we r here 2');
         pipeline = [
        {
          '$match': {
            'year': {'$eq': year}
          }
        }, 
        {
          '$group': {'_id': '$requeststatus','count': {'$sum': 1}}
        }
      ];


    }
    else if(filterData['type']=='total')
    {
        console.log('we r here 3');
         pipeline = [ 
        {
          '$group': {'_id': '$requeststatus','count': {'$sum': 1}}
        }
      ];

    }
    else if(filterData['type']=='day')
    {

        console.log('we r here 3');
         pipeline = [
        {
          '$match': {
            '$and':[{'month': {'$eq': month}},{'year': {'$eq': year}},{'day': {'$eq': day}}]
          }
        }, 
        {
          '$group': {'_id': '$requeststatus','count': {'$sum': 1}}
        }
      ];

    }
    else
    {
        console.log('we r here 4');
         pipeline = [
        {
          '$match': {
            'updatedon': {'$gte': tempdate}
          }
        }, 
        {
          '$group': {'_id': '$requeststatus','count': {'$sum': 1}}
        }
      ];
    }

    console.log(pipeline);
    return pipeline;
};

const findDocumentsDashboard = async function(mongocollection, filterData, callback) {
    console.log('findDocuments:filterData',filterData);
    
    //searchData
    let searchData = simplifyFilterDataDashboard(filterData);
    //console.log('findDocuments:filterData',filterData);
    
    try{
        console.log('findDocuments:searchData', searchData);
        await mongocollection.aggregate(searchData).toArray(function(err, docs) {
            assert.equal(err, null);
            if(docs.length <1){return callback(mongoDbErrorCodes(7508));}
            else{callback(docs);}
        });
    }catch (error) {
        console.log('findDocuments:Exception',error);
        let errorData = mongoDbErrorCodes(7505);
        errorData.reason = error;
        return callback(errorData);
    }  
};

const dashboardMongoDbHandler = function(queryString,eachItem,key) {
    return new Promise( (resolve,reject) =>
    {
        console.log('listMongoDbHandler:queryString',queryString);
        try {
            console.log('listMongoDbHandler:TRY',queryString);
            MongoClient.connect(MongodbUrl, MongodbOptions, function(err, client) {
                console.log(`err mongodb ====> ${JSON.stringify(err)}`);
                assert.equal(null, err);
                console.log('Connected successfully to Mongo Db server');
                const mongodbcon = client.db(MongodbName);
                const mongocollection = mongodbcon.collection(queryString['providerid']);
                findDocumentsDashboard(mongocollection,queryString,function(doclist){
                    console.log('doclist===>',doclist);

                    if(queryString['summary'])
                    {
                        if(doclist.errorcode)
                        {
                            
                                eachItem.index=queryString['type'];
                                eachItem.totalcount=0;
                                eachItem.newcount=0;
                                eachItem.opencount=0;
                                eachItem.closedcount=0;
                            
                        }
                        else
                        {
                            eachItem['index']=queryString['type'];
                            for (let index in doclist) {
                                    //tempdata[doclist[index].status]=doclist[index].count;
                                    if(doclist[index]['_id']=='CREATED')
                                    {
                                        eachItem['newcount']=doclist[index]['count'];
                                    }
                                    else if(doclist[index]['_id']=='ASSIGNED')
                                    {
                                        eachItem['opencount']=doclist[index]['count'];
                                    }
                                    else if(doclist[index]['_id']=='CLOSED')
                                    {
                                        eachItem['closedcount']=doclist[index]['count'];
                                    }
                                    //eachItem[doclist[index]['_id']['requeststatus']]=doclist[index]['_id']['count'];
                                }
                                //eachItem[]=tempdata;
                                if(!eachItem['newcount'])
                                {
                                    eachItem['newcount']=0;
                                }
                                if(!eachItem['opencount'])
                                {
                                    eachItem['opencount']=0;
                                }
                                if(!eachItem['closedcount'])
                                {
                                    eachItem['closedcount']=0;
                                }
                        }
                            
                    }
                    else
                    {
                         if(doclist.errorcode)
                            {
                                //eachItem.push({'index':key,'newcount':0,'opencount':0,'closedcount':0});
                                eachItem.newcount=0;
                                eachItem.opencount=0;
                                eachItem.closedcount=0;
                            }
                            else
                            {
                                //let tempdata={};
                                for (let index in doclist) {
                                    //tempdata[doclist[index].status]=doclist[index].count;
                                    if(doclist[index]['_id']=='CREATED')
                                    {
                                        eachItem['newcount']=doclist[index]['count'];
                                    }
                                    else if(doclist[index]['_id']=='ASSIGNED')
                                    {
                                        eachItem['opencount']=doclist[index]['count'];
                                    }
                                    else if(doclist[index]['_id']=='CLOSED')
                                    {
                                        eachItem['closedcount']=doclist[index]['count'];
                                    }
                                    //eachItem[doclist[index]['_id']['requeststatus']]=doclist[index]['_id']['count'];
                                }
                                //eachItem[]=tempdata;
                                if(!eachItem['newcount'])
                                {
                                    eachItem['newcount']=0;
                                }
                                if(!eachItem['opencount'])
                                {
                                    eachItem['opencount']=0;
                                }
                                if(!eachItem['closedcount'])
                                {
                                    eachItem['closedcount']=0;
                                }
                            }
                    }

                   
                    console.log('eachItem===>',eachItem);
                    return resolve(eachItem);
                });
            });
        } catch (error) {
            console.log('listMongoDbHandler:Exception',error);
            let errorData = mongoDbErrorCodes(7505);
            errorData.reason = error;
            return reject(errorData);
        }
    })
};

module.exports = {
    insertManyMongoDbHandler: insertManyMongoDbHandler,
    getMongoDbHandler: getMongoDbHandler,
    listMongoDbHandler: listMongoDbHandler,
    dashboardMongoDbHandler: dashboardMongoDbHandler,
    updateMongoDbHandler: updateMongoDbHandler
};