'use strict';
let FormHandling = require('../common/FormHandling');

const redisErrorCodes = function(errorcode){
    let errorlist = {};
    errorlist['7201'] = 'Redis Connection Failed';
    errorlist['7202'] = 'Redis Pool Failed';
    errorlist['7203'] = 'Redis Set Failed';
    errorlist['7204'] = 'Redis Get Failed';
    return FormHandling.formatErrorCode(errorcode, errorlist);
};

var getRedisKey = function (redisClient,redisKey,callback){
    try {
        redisClient.get(redisKey, function(error, redisData){
            if(error){let errorData = redisErrorCodes(7204);errorData.reason = error;return callback(errorData);
            }else{return callback(redisData);}
        });
    }catch (error) {
        //console.log('getRedisKey:Exception',error);
        let errorData = redisErrorCodes(7204);
        errorData.reason = error;
        return callback(errorData);
    }
};

var setRedisKey = function (redisClient,redisKey,redisValue,callback){
    try {
        redisClient.set(redisKey, redisValue, function(error, redisData){
            if(error){let errorData = redisErrorCodes(7203);errorData.reason = error;return callback(errorData);
            }else{return callback(redisData);}
        }); 
    }catch (error) {
        //console.log('setRedisKey:Exception',error);
        let errorData = redisErrorCodes(7203);
        errorData.reason = error;
        return callback(errorData);
    }
};

var setRedisKeyExpiry = function (redisClient, redisKey, redisValue, expirySecond, callback){
    try {
        redisClient.set(redisKey, redisValue, function(error, redisData){
            if(error){let errorData = redisErrorCodes(7203);errorData.reason = error;return callback(errorData);}
            if(!isNaN(expirySecond) && expirySecond > 0) {redisClient.expire(redisKey, parseInt(expirySecond));}
            return callback(redisData);
        });
    }catch (error) {
        //console.log('setRedisKey:Exception',error);
        let errorData = redisErrorCodes(7203);
        errorData.reason = error;
        return callback(errorData);
    }
};

module.exports = {
    getRedisKey:getRedisKey,
    setRedisKey:setRedisKey,
    setRedisKeyExpiry: setRedisKeyExpiry
};