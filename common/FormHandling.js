'use strict';
var convertStringToJson = function(inputdata){
    try{
        //Already Object
        if(typeof(inputdata) === 'object'){return inputdata;}

        //String To JSON
        if((typeof(inputdata) === 'string') && (JSON.parse(inputdata))){return JSON.parse(inputdata);}

        //Default False
        return false;
    }
    catch(error){
        return false;
    }
};

var formatErrorCode = function(errorcode, errorlist){
    //Error Codes
    let reason = errorlist[errorcode] || 'Unknown Error Occured';
    let finalResponse = {};
    finalResponse.errorcode = errorcode;
    finalResponse.reason = reason;

    //Final Response
    console.log('formatErrorCode',finalResponse);
    return finalResponse;
};

var reportFilterToQuerystring = function(queryString, reportFilter){
    reportFilter = convertStringToJson(reportFilter);
    for(let thiskey in reportFilter){let thisrowdata = reportFilter[thiskey];if((thisrowdata['name']) && (thisrowdata['value'])){queryString[thisrowdata['name']] = thisrowdata['value'];}}
    delete queryString.filter;
    return queryString;
};

var getDashboardPeriodType = function(queryString){
    let periodType = 'day';
    var periodList = ['day', 'week', 'month', 'year'];
    if((queryString['period']) && (periodList.indexOf(queryString['period']) >= 0)){periodType = queryString['period'];}
    return periodType;
};

module.exports = {
    convertStringToJson: convertStringToJson,
    formatErrorCode: formatErrorCode,
    reportFilterToQuerystring: reportFilterToQuerystring,
    getDashboardPeriodType: getDashboardPeriodType
};