'use strict';
const redis = require('redis');
const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });

//console.log('RedisPoolConnection:init');
let redis_config = {};
redis_config.host = process.env.REDIS_HOST || 'localhost';;
redis_config.port = process.env.REDIS_PORT || 6379;
redis_config.auth_pass = process.env.REDIS_PASSWORD || '';
//console.log('RedisPoolConnection:redis_config',redis_config);

//Create RedisPoolConnection
const redisClient = redis.createClient(redis_config);
redisClient.on('error', function(status){console.error('RedisPoolConnection', new Date(), 'Redis_Error', status);});
redisClient.on('connect', function(){console.error('RedisPoolConnection', new Date(), 'Redis_Connected');});
redisClient.on('end', function(){console.error('RedisPoolConnection', new Date(), 'Redis_End');});
module.exports = redisClient;