const express = require('express');
const router = express.Router();

var dashboardapi = require('../utilities/dashboardAPI');
router.get('/v1/dashboard', dashboardapi.dashboardSummaryHandler);

module.exports = router;